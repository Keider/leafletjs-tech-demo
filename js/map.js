//location vars
var z = 14;
var x = 46.781980;
var y = -71.274814;
var styleId = 997; //style of the tiles
var s = 'b'; //sub-domain of the tile server

//leaflet init vars
var CM_URL = 'http://{s}.tile.cloudmade.com/d4fc77ea4a63471cab2423e66626cbb6/{styleId}/256/{z}/{x}/{y}.png';
var CM_ATTR = 'Projet GLO 2014';

//init leaflet
var map = L.map('map').setView([x, y], z); 
L.tileLayer(CM_URL, {attribution: CM_ATTR, styleId: styleId}).addTo(map);

//single polygon
/*var uniOuest = L.polygon([
	[46.78301,-71.28563],[46.786162,-71.279708],[46.787102,-71.276511],[46.785163,-71.275502],[46.779991,-71.269365],[46.782371,-71.264452],[46.779477,-71.264816],[46.774083,-71.275373]
 ]).addTo(map);
var uniEst = L.polygon([
	[46.787102,-71.276511],[46.785163,-71.275502],[46.779991,-71.269365],[46.782371,-71.264452],[46.782944,-71.264108],[46.783561,-71.262907],[46.785236,-71.264967],[46.784810,-71.265825],[46.786603,-71.268228],[46.786500,-71.268743],[46.786764,-71.269816],[46.788307,-71.271940]
]).addTo(map);*/

//choropleth
var geojson;

//utility functions
function getColor(d) {
    return d > 1000 ? '#800026' :
           d > 500  ? '#BD0026' :
           d > 200  ? '#E31A1C' :
           d > 100  ? '#FC4E2A' :
           d > 50   ? '#FD8D3C' :
           d > 20   ? '#FEB24C' :
           d > 10   ? '#FED976' :
                      '#FFEDA0';
}

function style(feature)
{
    return {
        fillColor: getColor(feature.properties.density),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}

//listeners
function highlightFeature(e) {
    var layer = e.target;

    layer.setStyle({
        weight: 5,
        color: '#666',
        dashArray: '',
        fillOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
    }
    
    info.update(layer.feature.properties);
}

function resetHighlight(e) {
    geojson.resetStyle(e.target);
    info.update();
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

//custom control
var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info topRight'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    this._div.innerHTML = '<h4>Index arbitraire : </h4>' +  (props ?
        '<b>' + props.name + '</b><br />' + props.density + ' unité<sup>2</sup>'
        : 'Survolez une région');
};

info.addTo(map);

//legend
var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = [0, 10, 20, 50, 100, 200, 500, 1000],
        labels = [];

    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }

    return div;
};

legend.addTo(map);

//init
geojson = L.geoJson(choroplethData, {
    style: style,
    onEachFeature: onEachFeature
}).addTo(map);

/*geojson = L.geoJson(statesData, {
    style: style,
    onEachFeature: onEachFeature
}).addTo(map);*/